# Hardclimbz

Hardclimbz is a Django web application for tracking climbing goals.

## Running

Hardclimbz requires:

1. Python 3.5+
2. Django 1.10.5
3. Sass

```sh
$ python -m venv .
$ source bin/activate
$ pip install -r requirements.txt
$ cd hardclimbz
$ npm install // install Node dependencies
$ gulp styles // Generate CSS
$ python manage.py runserver // Start app
```

The application will be running on 127.0.0.1:8000.

## Styles

A default Gulp target to watch the `sass` directory and recompile
after any changes is provided. Simply run `gulp` and let it stay running.

## Testing

The full test suite can be run with `python manage.py test`. Tests for
individual packages (or apps) can be run with `python manage.py test package_name`.

### Functional / E2E Tests

Hardclimbz has end-to-end tests in the `functional_tests` package. These
tests use [Selenium](https://pypi.python.org/pypi/selenium) and
FirefoxDeveloperEdition.The [Firefox WebDriver](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/WebDriver)
must be in your `$PATH` for e2e tests to work.
