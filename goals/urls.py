from django.conf.urls import url

from . import views


app_name = 'goals'
urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/$', views.goal_detail, name='goal_detail'),
    url(r'^(?P<pk>[0-9]+)/complete$', views.goal_complete,
        name='goal_complete'),
    url(r'^add/$', views.add_goal, name='add_goal'),
    url(r'(?P<pk>[0-9]+)/delete$', views.delete_goal, name='delete_goal'),
]
