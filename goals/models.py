from django.db import models


class Goal(models.Model):
    """
    A single goal.
    """
    goal_title = models.CharField(max_length=200)
    goal_description = models.TextField()
    date_due = models.DateField()
    date_completed = models.DateField(blank=True, null=True)

    def completed(self):
        return self.date_completed is not None
    completed.boolean = True

    def completed_on_time(self):
        if not self.completed():
            return False
        return self.date_completed.date() <= self.date_due.date()
    completed_on_time.boolean = True

    def __str__(self):
        return self.goal_title


class Note(models.Model):
    """
    A note relating to a single :model:`.Goal`.
    """
    goal = models.ForeignKey('Goal', on_delete=models.CASCADE)
    note_text = models.TextField()
    date = models.DateTimeField(auto_now=True)
