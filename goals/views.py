from datetime import date

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.dateparse import parse_date
from django.utils.translation import ugettext_lazy as _

from .models import Goal, Note


def goal_detail(request, pk):
    goal = get_object_or_404(Goal, pk=pk)

    if request.method == 'POST':
        note = request.POST['note-text']
        Note.objects.create(goal=goal, note_text=note)
        return HttpResponseRedirect(reverse('goals:goal_detail', args=(pk,)))

    return render(request, 'goals/goal_detail.html', {'goal': goal})


def goal_complete(request, pk):
    goal = get_object_or_404(Goal, pk=pk)
    date_completed = parse_date(request.POST['date-completed'])
    goal.date_completed = date_completed
    goal.save()

    return HttpResponseRedirect(reverse('goals:goal_detail', args=(pk,)))


def add_goal(request):
    goal_title = request.POST['goal-title']
    goal_description = request.POST['goal-description']
    date_due = request.POST['date-due']

    if parse_date(date_due) < date.today():
        messages.error(request,
                       _('Goals cannot have due dates earlier than today.'))
        return HttpResponseRedirect('/dashboard/')

    goal = Goal.objects.create(goal_title=goal_title,
                               goal_description=goal_description,
                               date_due=date_due)

    return HttpResponseRedirect(reverse('goals:goal_detail', args=(goal.id,)))


def delete_goal(request, pk):
    goal = get_object_or_404(Goal, pk=pk)
    messages.info(request, _('Deleted %s' % goal.goal_title))
    goal.delete()

    return HttpResponseRedirect('/dashboard/')
