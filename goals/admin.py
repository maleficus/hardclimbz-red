from django.contrib import admin

from .models import Goal, Note


class NoteInline(admin.StackedInline):
    model = Note
    extra = 1


class GoalAdmin(admin.ModelAdmin):
    inlines = [NoteInline]
    list_display = ('goal_title', 'completed')


admin.site.register(Goal, GoalAdmin)
admin.site.register(Note)
