import datetime

from django.test import TestCase
from django.utils import timezone

from .models import Goal


class GoalMethodTests(TestCase):

    def test_completed_with_incomplete_goal(self):
        """
        completed() should return False for a goal that does not
        have a date_completed date.
        """
        date_due = timezone.now() + datetime.timedelta(days=30)
        goal = Goal(goal_title="New goal",
                    goal_description="A new goal to complete.",
                    date_due=date_due)
        self.assertIs(goal.completed(), False)

    def test_completed_with_complete_goal(self):
        """
        completed() should return True for a goal that has a
        date_completed date.
        """
        date_due = timezone.now() + datetime.timedelta(days=30)
        date_completed = timezone.now() + datetime.timedelta(days=5)
        goal = Goal(goal_title="New goal",
                    goal_description="A new goal to complete.",
                    date_due=date_due,
                    date_completed=date_completed)
        self.assertIs(goal.completed(), True)

    def test_completed_on_time_with_incomplete_goal(self):
        """
        completed_on_time() should return False for a goal that does
        not have a date_completed date.
        """
        date_due = timezone.now() + datetime.timedelta(days=30)
        goal = Goal(goal_title="New goal",
                    goal_description="A new goal to complete.",
                    date_due=date_due)
        self.assertIs(goal.completed_on_time(), False)

    def test_completed_on_time_with_complete_and_early_goal(self):
        """
        completed_on_time() should return True for a goal that has a
        date_completed date that was earlier than its date_due date.
        """
        date_due = timezone.now() + datetime.timedelta(days=30)
        date_completed = timezone.now() + datetime.timedelta(days=5)
        goal = Goal(goal_title="New goal",
                    goal_description="A new goal to complete.",
                    date_due=date_due,
                    date_completed=date_completed)
        self.assertIs(goal.completed_on_time(), True)

    def test_completed_on_time_with_complete_and_late_goal(self):
        """
        completed_on_time() should return False for a goal that has a
        date_completed date that was later than its date_due date.
        """
        date_due = timezone.now() + datetime.timedelta(days=30)
        date_completed = timezone.now() + datetime.timedelta(days=40)
        goal = Goal(goal_title="New goal",
                    goal_description="A new goal to complete.",
                    date_due=date_due,
                    date_completed=date_completed)
        self.assertIs(goal.completed_on_time(), False)

    def test_completed_on_time_with_goal_completed_on_due_date(self):
        """
        completed_on_time() should return True for a goal that has a
        date_completed date that is the same as its due date.
        """
        date_due = timezone.now() + datetime.timedelta(days=30)
        date_completed = timezone.now() + datetime.timedelta(days=30)
        goal = Goal(goal_title="New goal",
                    goal_description="A new goal to complete.",
                    date_due=date_due,
                    date_completed=date_completed)
        self.assertIs(goal.completed_on_time(), True)
