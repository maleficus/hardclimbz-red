# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-08 22:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goals', '0002_auto_20170108_1629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goal',
            name='date_completed',
            field=models.DateField(blank=True, null=True),
        ),
    ]
