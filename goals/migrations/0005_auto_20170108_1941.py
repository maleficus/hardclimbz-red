# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-09 01:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goals', '0004_note_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goal',
            name='goal_description',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='note',
            name='note_text',
            field=models.TextField(),
        ),
    ]
