from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


class DashboardIndexTest(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(DashboardIndexTest, cls).setUpClass()
        binary = FirefoxBinary(
            '/Applications/FirefoxDeveloperEdition.app/Contents/MacOS/firefox'
        )
        cls.browser = webdriver.Firefox(firefox_binary=binary)
        cls.browser.implicitly_wait(3)

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(DashboardIndexTest, cls).tearDownClass()

    def test_dashboard_route_goes_to_dashboard(self):
        self.browser.get('%s%s' % (self.live_server_url, '/dashboard/'))
        body_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('Dashboard', body_text)

    def test_an_error_is_displayed_on_index_after_invalid_date_input(self):
        self.browser.get('%s%s' % (self.live_server_url, '/dashboard/'))

        goal_title_input = self.browser.find_element_by_id('goal_title')
        goal_title_input.send_keys('Pass This Test')

        date_due_input = self.browser.find_element_by_id('date_due')
        date_due_input.send_keys('2007-04-16')

        goal_description_input = self.browser.find_element_by_id(
            'goal_description'
        )
        goal_description_input.send_keys('Hope this test passes!')

        self.browser.find_element_by_id('add_goal').click()
        article = self.browser.find_element_by_tag_name('article').text

        self.assertIn('Goals cannot have due dates earlier than today.',
                      article)
