from django.shortcuts import render

from goals.models import Goal


def dashboard_index(request):
    incomplete_goals_ids = [goal.id for goal in Goal.objects.all()
                            if not goal.completed()]
    incomplete_goals = Goal.objects.filter(id__in=incomplete_goals_ids) \
                                   .order_by('date_due')

    completed_goals_ids = [goal.id for goal in Goal.objects.all()
                           if goal.completed()]
    completed_goals = Goal.objects.filter(id__in=completed_goals_ids) \
                                  .order_by('-date_completed')

    goals_count = Goal.objects.all().count()
    completed_goals_count = len(completed_goals)

    context = {
        'goals_count': goals_count,
        'completed_goals_count': completed_goals_count,
        'goals_due_soon': incomplete_goals[:3],
        'recently_completed_goals': completed_goals[:3],
    }
    return render(request, 'dashboard/index.html', context)
