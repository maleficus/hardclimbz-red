var gulp = require('gulp');
var sass = require('gulp-sass');

// Compile Sass
gulp.task('styles', function() {
    return gulp.src('sass/*.sass')
        .pipe(sass({
            includePaths: ['./node_modules/bulma/']
        }).on('error', sass.logError))
        .pipe(gulp.dest('./static/'));
});

// Watch Sass directory for changes and then compile
gulp.task('default', function() {
    gulp.watch('sass/*.sass', ['styles']);
});
