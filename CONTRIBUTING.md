# Contribute to HardClimbz

## Code Style

Code should follow [Django's recommended style](https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/).
This boils down to following [PEP 8](https://www.python.org/dev/peps/pep-0008/),
[PEP 257](https://www.python.org/dev/peps/pep-0257/) and intelligently organizing imports.

`flake8` can and should be used for verifying compliance with PEP 8 and `isort` can be used
to organize imports with `isort -rc .`.
